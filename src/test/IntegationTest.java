import com.softtek.beans.Person;
import com.softtek.configuration.JDBCConfiguration;
import com.softtek.dao.PersonDao;
import com.softtek.dao.PersonRepository;
import com.softtek.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JDBCConfiguration.class})
@WebAppConfiguration
public class IntegationTest {
    @Autowired
    PersonService personService;

    @Test
    public void getByIdIntegrateTest(){
        //setup
        Person person;
        Person correctPerson = new Person("Noel",16,4);
        //execute
        person = personService.updatePerson(4);
        //validate
        assertEquals(correctPerson.getName(),person.getName());
    }
}
