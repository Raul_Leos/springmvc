package com.softtek.controller;

import com.softtek.beans.Person;
import com.softtek.dao.PersonDao;
import com.softtek.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import java.util.List;

@Controller
public class PersonController {
    @Autowired
    PersonService personService;

    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public ModelAndView student(){
        return new ModelAndView("person","command", new Person());
    }

    @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public ModelAndView addPerson(@ModelAttribute("SpringWeb")Person person){
        personService
                .addPerson(person);
        return new ModelAndView("redirect:" +"http://localhost:8080/SpringMVCApp/viewPersons");
    }

    @RequestMapping(value = "/viewPersons", method = RequestMethod.GET)
    public String viewPerson(Model m){
        List<Person> list = personService.getAll();
        m.addAttribute("list",list);
        return "viewemp";
    }

    @RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
    public ModelAndView deletePerson(@PathVariable int id){
        personService.deletePerson(id);
        return new ModelAndView("redirect:" +"http://localhost:8080/SpringMVCApp/viewPersons");
    }

    @RequestMapping(value = "/updatePerson/{id}", method = RequestMethod.GET)
    public ModelAndView updatePerson(@PathVariable int id, Model m){
        Person person = personService.updatePerson(id);
        m.addAttribute(
                "person", person
        );
        return new ModelAndView("modifyPerson","command", new Person());
    }

    @RequestMapping(value = "/modifyPerson", method = RequestMethod.POST)
    public ModelAndView modifyPerson(@ModelAttribute("SpringWeb") Person person){
       personService.updatePersonByObject(person);
        return new ModelAndView("redirect:" +"http://localhost:8080/SpringMVCApp/viewPersons");
    }
}
