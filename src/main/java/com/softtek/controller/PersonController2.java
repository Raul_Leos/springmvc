package com.softtek.controller;

import java.util.List;

import com.softtek.beans.Person;
import com.softtek.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("persons")
public class PersonController2 {

    @Autowired
    PersonService personService;

    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public String viewPerson(Model m){
        List<Person> list = personService.getAll();
        m.addAttribute("list",list);
        return "viewemp";
    }

}
