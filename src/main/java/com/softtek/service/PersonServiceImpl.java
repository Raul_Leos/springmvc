package com.softtek.service;

import com.softtek.beans.Person;
import com.softtek.dao.PersonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    @Qualifier("personRepository")
    private PersonDao personDao;

    @Override
    public List<Person> getAll() {
        return personDao.getAll();
    }

    @Override
    public void addPerson(Person person) {
        personDao.addPerson(person);
    }

    @Override
    public Person updatePerson(int id) {
        return personDao.updatePerson(id);
    }

    @Override
    public void deletePerson(int id) {
        personDao.deletePerson(id);
    }

    @Override
    public void updatePersonByObject(Person person) {
        personDao.updatePersonByObject(person);
    }
}
