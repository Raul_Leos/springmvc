package com.softtek.service;
import java.sql.Connection;
import java.sql.DriverManager;
public class DbConnection {
    Connection con;

    public void conectDb(){
        final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        final String DB_URL = "jdbc:mysql://localhost:3306/jsp"
                + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        final String USER = "root";
        final String PASS = "1234";

        try {
            Class.forName(JDBC_DRIVER);
            this.con = DriverManager.getConnection(DB_URL,USER,PASS);
        }catch(Exception e) {
            System.out.println(e);
        }
    }

    public Connection getConection() {

        if(this.con!=null)
            return this.con;
        else
            return null;
    }
}

