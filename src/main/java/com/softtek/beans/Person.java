package com.softtek.beans;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person {
    @Id
    private int idPerson;
    private String name;
    private int age;


    public Person(){}

    public Person(String name, int age, int id) {
        this.name = name;
        this.age = age;
        this.idPerson = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return idPerson;
    }

    public void setId(int id) {
        this.idPerson = id;
    }
}
