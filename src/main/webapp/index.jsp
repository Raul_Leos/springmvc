<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Students</title>
</head>
<body>
<script>
    window.onload = function () {
        window.location.href = "http://localhost:8080/SpringMVCApp/viewPersons";
    };
</script>
    <div class="container">
        <h1>Student Information</h1>
        <form action="" class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" class="form-control">
            <label for="age">Age:</label>
            <input type="text" name="age" id="age" class="form-control">
            <label for="id">id:</label>
            <input type="text" name="id" id="id" class="form-control">
            <div align="center">
                <button class="btn btn-primary mt-3" type="submit">Submit</button>
            </div>
        </form>
    </div>
</body>
</html>
