<%--
  Created by IntelliJ IDEA.
  User: raul.leos
  Date: 12/2/2019
  Time: 4:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri ="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Persons</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="mt-5" align="center">
            <h1 class="mb-5">Persons List</h1>
            <a  href="http://localhost:8080/SpringMVCApp/person" class=" mb-3 btn btn-success">Add Person</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Age</th>
                <th scope="col">Delete</th>
                <th scope="col">Update</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="person" items="${list}">
                <tr>
                    <td scope="row">${person.id}</td>
                    <td>${person.name}</td>
                    <td>${person.age}</td>
                    <td>
                        <form action="http://localhost:8080/SpringMVCApp/deletePerson/${person.id}" method="get">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                    <td>
                        <form action="http://localhost:8080/SpringMVCApp/updatePerson/${person.id}" method="get">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</body>
</html>
