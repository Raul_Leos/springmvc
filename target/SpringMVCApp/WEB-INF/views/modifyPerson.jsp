<%--
  Created by IntelliJ IDEA.
  User: raul.leos
  Date: 11/27/2019
  Time: 4:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri ="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Modify Person</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Update Student Information</h1>
    <form:form method="post" action="http://localhost:8080/SpringMVCApp/modifyPerson" class="form-group">
        <form:label for="name" path="name" >Name:</form:label>
        <form:input path="name" type="text" name="name" id="name" class="form-control" value="${person.name}"/>
        <form:label path="age" for="age">Age:</form:label>
        <form:input path="age" type="text" name="age" id="age" class="form-control" value="${person.age}"/>
        <form:label path="id" for="id">id:</form:label>
        <form:input path="id" type="text" name="id" id="id" class="form-control" value="${person.id}"/>
        <div align="center">
            <button class="btn btn-primary mt-3" type="submit">Submit</button>
        </div>
    </form:form>
</div>
</body>
</html>
